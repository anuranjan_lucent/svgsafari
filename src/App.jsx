import { useEffect, useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";

function App() {
  const [change, setChange] = useState("");

  let str = `<svg viewBox="0 0 1000 1000" xmlns="http://www.w3.org/2000/svg">


         <text id="one" y="90%" fill="blue" font-size="100px">THIS IS TO DEMONSTRATE THE SQUEEZING </text>


         <path d="M 144 489 A 360 360 0 0 1 864  489" id="pathThree"></path>
         <path d="M 200 600 L 700 600" id="pathTwo"></path>


        <text fill="blue" font-size="100px">
            <textPath id="two" lengthAdjust="spacingAndGlyphs" href="#pathTwo" textLength="500px">THIS IS TO DEMONSTRATE THE SQUEEZING </textPath>
        </text>

        <text fill="blue" font-size="100px">
            <textPath id="three" startOffset="50%" lengthAdjust="spacingAndGlyphs" href="#pathThree" textLength="1130px">THIS IS TO DEMONSTRATE THE SQUEEZING </textPath>
        </text>
  
</svg>`;

  useEffect(() => {
    let element = document.getElementById("dummy");
    if (element) {
      element.remove();
    }
    const htmlContent = `
    <!DOCTYPE html>
    <html>
    <head>
        <style>
            body {
                margin: 0;
                padding: 0;
                display: block;
                justify-content: center;
                align-items: center;
                height: 100vh;
                background-color: #f0f0f0;
            }
        </style>
    </head>
    <body>
        <svg viewBox="0 0 1000 1000" xmlns="http://www.w3.org/2000/svg">


         <text id="one" y="90%" fill="blue" font-size="100px">${change}</text>


         <path d="M 144 489 A 360 360 0 0 1 864  489" id="pathThree"></path>
         <path d="M 200 600 L 700 600" id="pathTwo"></path>


        <text id="twoText"  text-anchor="middle"  fill="blue" font-size="100px" lengthAdjust="spacingAndGlyphs">
            <textPath id="two"  text-anchor="start" startOffset="0%" lengthAdjust="spacingAndGlyphs" href="#pathTwo">${change}</textPath>
        </text>

        <text  id="threeText" text-anchor="middle"   fill="blue" font-size="100px" lengthAdjust="spacingAndGlyphs">
            <textPath id="three" text-anchor="middle" startOffset="50%" lengthAdjust="spacingAndGlyphs" href="#pathThree" >${change}</textPath>
        </text>
  
</svg>
    </body>
     <script>
        window.onload = function () {
       
            const textOne = document.getElementById("one");
            const textTwo = document.getElementById("two");
            const textThree = document.getElementById("three");
            const textTagTwo = document.getElementById("twoText");
            const textTagThree = document.getElementById("threeText");
            const width = textOne.getComputedTextLength();

         
            if (width > 500) {
                textTwo.setAttribute("textLength", "500px");
                 textTagTwo.setAttribute("textLength", "500px");
                 
             
                 
            } else{
              textTwo.removeAttribute("textLength");
             textTagTwo.removeAttribute("textLength");

             }
            if (width > 1130) {
               
                textThree.setAttribute("textLength", "1130px");
                textTagThree.setAttribute("textLength", "1130px");
                 
            } else{
              textThree.removeAttribute("textLength");
            textTagThree.removeAttribute("textLength")
             }
        };
    </script>
    </html>
`;

    // Step 2: Convert HTML content to a data URL
    const dataUrl = `data:text/html;charset=utf-8,${encodeURIComponent(
      htmlContent
    )}`;

    console.log("Dataurl", dataUrl);

    const blob = new Blob([htmlContent], { type: "text/html" });

    // Create a Blob URL from the Blob object
    const blobURL = URL.createObjectURL(blob);

    console.log("bloburl", blobURL);

    const iframe = document.createElement("iframe");
    iframe.style.width = "1000px";
    iframe.style.height = "1000px";
    iframe.setAttribute("id", "dummy");
    iframe.style.border = "none";
    iframe.sandbox = "allow-same-origin allow-scripts";
    iframe.src = blobURL;
    console.log("Blob was changed");
    // Step 4: Append iframe to the document body
    document.body.appendChild(iframe);
  }, [change]);

  return (
    <div
      style={{
        fontSize: "50px",
        fontColor: "blue",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <div>
        <input
          onChange={(e) => setChange(e.target.value.toLocaleUpperCase())}
        />
        This is my apppp
      </div>
      <div>Man made</div>
    </div>
  );
}

export default App;
